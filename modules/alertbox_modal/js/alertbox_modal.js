(function ($) {
    Drupal.behaviors.alertbox_modal = {
        attach: function (context, settings) {
            var content = '';
            var alertbox_ids = [];

            // Get all the alertboxes id's and wrap content for display.
            $('.block-nodeblock .node-alertbox .content').each(function () {
                alertbox_id = $(this).parents('.node-alertbox').attr('id');
                alertbox_ids.push(alertbox_id);
                // Check cookie status. If cookie set, we don't show the alert.
                status = Drupal.alertbox.getCurrentStatus(alertbox_id);
                if (!status || !Drupal.settings.alertbox.allow_hiding_alertbox) {
                    text = $(this).wrapInner('<div class="node-alertbox-row"></div>').html();
                    content += text;
                }
            });

            // If no content to show, we don't have any alerts currently.
            if (content == '') {
                return;
            }

            // Initializing the modal.
            Drupal.CTools.Modal.show('alertbox_modal');
            $('#modal-title').html('');
            $('#modal-content').html(content).scrollTop(0);

            // If Hiding Alerts is enabled, a cookie will be saved to avoid display the same alert again.
            if (Drupal.settings.alertbox.allow_hiding_alertbox) {
                // Bind a click when closing the modalContent
                $('.modal-alertbox .modal-header .close').bind('click', function (e) {
                    alertbox_ids.forEach(function (alertbox_id) {
                        Drupal.alertbox.setStatus(alertbox_id);
                    });
                });
            }
        }
    }

    Drupal.behaviors.ctools_backdrop_close = {
        attach: function (context, settings) {
            // We add a Backdrop clicking event if we don't need to create a cookie to keep user choice.
            if (!Drupal.settings.alertbox.allow_hiding_alertbox) {
                $('#modalBackdrop').once('ctools_backdrop_close', function () {
                    $(this).click(function () {
                        Drupal.CTools.Modal.dismiss();
                    });
                });
            }
        }
    }
})(jQuery);
