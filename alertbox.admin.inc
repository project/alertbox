<?php

/**
 * @file
 * Administration page callbacks for the Alertbox module.
 */

/**
 * Form constructor to set Alertbox configuration.
 */
function alertbox_admin_settings_form($form, &$form_state) {

  $form['alertbox_interface_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Interface options'),
    '#weight' => -10,
    '#collapsible' => FALSE,
  );

  // Add a checkbox to enable/disable a Close button on alertboxs
  $form['alertbox_interface_options']['alertbox_allow_hide'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow user to hide alert boxes'),
    '#default_value' => variable_get('alertbox_allow_hide', FALSE),
    '#description' => t('By checking this option a cookie will be stored at user side to keep a hidden state.'),
  );

  // Get default active theme.
  $default_theme_key = variable_get('theme_default', '');
  // Get theme regions.
  $default_theme_regions = system_region_list($default_theme_key, REGIONS_VISIBLE);
  $form['alertbox_' . $default_theme_key . '_region'] = array(
    '#type' => 'select',
    '#title' => t('Pre-defined region for @themename theme', array('@themename' => $default_theme_key)),
    '#options' => $default_theme_regions,
    '#default_value' => variable_get('alertbox_' . $default_theme_key . '_region', 'content'),
    '#description' => t('Set the default theme region where Alertbox blocks should be assigned to.'),
  );

  // Let's create a fieldset to define non-default theme regions.
  // These regions are disabled by default.
  $form['alertbox_theme_regions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other themes'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description' => t('Set each theme region where Alertbox blocks should be assigned to.'),
  );
  $theme_list = list_themes();
  foreach ($theme_list as $theme_key => $theme_info) {
    if ($theme_key == $default_theme_key) {
      continue;
    }
    $theme_regions = system_region_list($theme_key, REGIONS_VISIBLE);
    $theme_regions[-1] = t('Disabled');
    $form['alertbox_theme_regions']['alertbox_' . $theme_key . '_region'] = array(
      '#type' => 'select',
      '#title' => t('Pre-defined region for @themename theme', array('@themename' => $theme_info->name)),
      '#options' => $theme_regions,
      '#default_value' => variable_get('alertbox_' . $theme_key . '_region', BLOCK_REGION_NONE),
    );
  }

  return system_settings_form($form);
}

