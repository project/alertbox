<?php

/**
 * @file node-nodeblock-alertbox.tpl.php
 *
 * Theme implementation to display a nodeblock enabled block with a specific
 * style got Alertbox content type.
 *
 * Additional variables:
 * - $nodeblock: Flag for the nodeblock context.
 */
?>
<div id="alertbox-<?php print $node->nid; ?>"
     class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if ($show_close_button): ?>
    <div id="alertbox-buttons">
      <a href="#" class="hide-alertbox" role="button" aria-label="<?php print t('Close'); ?>"></a>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
    // We hide the comments and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    print render($content);
    ?>
  </div>

  <?php print render($content['links']); ?>


</div>
