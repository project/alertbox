<?php

/**
 * Implements hook_install().
 */
function alertbox_install() {
  node_types_rebuild();
  $types = node_type_get_types();
  // Add custom fields.
  add_custom_fields();
  // Enable nodeblock for this content type.
  variable_set('nodeblock_alertbox', 1);
  // Remove node preview.
  variable_set('node_preview_alertbox', 0);
  // Disable author and date display.
  variable_set('node_submitted_alertbox', 0);
  // Disable menu selection.
  variable_set('menu_options_alertbox', array());

  // If the site is multilingual, enable alertbox translations.
  if (module_exists('translation')) {
    variable_set('language_content_type_alertbox', 2);
    // Change display of language field.
    $alertbox_field_settings = variable_get('field_bundle_settings_node__alertbox', array());
    $alertbox_field_settings['extra_fields']['display']['language']['default']['visible'] = FALSE;
    $alertbox_field_settings['extra_fields']['display']['language']['default']['weight'] = 0;
    variable_set('field_bundle_settings_node__alertbox',$alertbox_field_settings);
    // It's strongly advised to use i18n_sync to avoid unexpected behaviours for
    // block positioning when changing field_alertbox_visibility.
    if (module_exists('i18n_sync')) {
      variable_set('i18n_sync_node_type_alertbox', array(
        'field_alertbox_visibility',
        'status'
      ));
    }
    else {
      drupal_set_message(t('Please enable @i18n_sync module and synchronize Alertbox Visibility field.', array(
        '@i18n_sync' => l('i18n_sync', 'admin/modules')
      )), 'warning');
    }
  }
}

/**
 * Implements hook_uninstall().
 */
function alertbox_uninstall() {
  $ournewtype = 'alertbox';
  $sql = 'SELECT nid FROM {node} n WHERE n.type = :type';
  $result = db_query($sql, array(':type' => $ournewtype));
  $nodeids = array();
  foreach ($result as $row) {
    $nodeids[] = $row->nid;
  }
  node_delete_multiple($nodeids);
  node_type_delete($ournewtype);
  field_purge_batch(500);
  variable_delete('nodeblock_alertbox');
}

/**
 * Helper that defines base fields.
 *
 * @return array
 *   base fields to add.
 */
function _alertbox_installed_fields() {
  $t = get_t();
  return array(
    'field_alertbox_visibility' => array(
      'field_name' => 'field_alertbox_visibility',
      'label' => $t('Block visibility'),
      'type' => 'list_text',
      'settings' => array('allowed_values_function' => 'get_visibility_options'),
      'cardinality' => -1
    ),
    'field_alertbox_body' => array(
      'field_name' => 'field_alertbox_body',
      'label' => $t('Body'),
      'type' => 'text_long',
      'entity_types' => array('node'),
    ),
  );
}

/**
 * Helper that defines field instances.
 *
 * @return array
 *   field instances with a default configuration.
 */
function _alertbox_installed_instances() {
  $t = get_t();
  return array(
    'field_alertbox_visibility' => array(
      'field_name' => 'field_alertbox_visibility',
      'type' => 'list_text',
      'label' => $t('Block visibility'),
      'widget' => array(
        'type' => 'options_buttons',
      ),
      'display' => array(
        'default' => array(
          'label' => $t('Block visibility'),
          'type' => 'hidden',
          'weight' => 0,

        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_summary_or_trimmed',
          'weight' => 1,
        ),
      )
    ),
    'field_alertbox_body' => array(
      'field_name' => 'field_alertbox_body',
      'label' => $t('Body'),
      'widget' => array(
        'type' => 'text_textarea'
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'type' => 'text_default',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'hidden',
          'type' => 'text_summary_or_trimmed',
          'weight' => 1,
        ),
      ),
    ),
  );
}

/**
 * Helper which creates fields and instances for alertbox bundle.
 */
function add_custom_fields() {
  foreach (_alertbox_installed_fields() as $field_base) {
    if (!field_info_field($field_base['field_name'])) {
      field_create_field($field_base);
    }
    else {
      field_update_field($field_base);
    }
  }
  foreach (_alertbox_installed_instances() as $field_instance) {
    $field_instance['entity_type'] = 'node';
    $field_instance['bundle'] = 'alertbox';
    if (!field_info_instance($field_instance['entity_type'], $field_instance['field_name'], $field_instance['bundle'])) {
      field_create_instance($field_instance);
    }
    else {
      field_update_instance($field_instance);
    }
  }
}
